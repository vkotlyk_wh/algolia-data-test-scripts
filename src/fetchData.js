const fs = require("fs");
const algoliasearch = require("algoliasearch");
const { saveDataForType } = require("./utils");

require("dotenv").config({ path: __dirname + "/../.env" });

function createFetchItems(client) {
  return async (indexName) => {
    const index = client.initIndex(indexName);
    let hits = [];
    await index.browseObjects({
      batch: (batch) => {
        hits = hits.concat(batch);
      },
    });

    return hits;
  };
}

async function run() {
  const apiKey = process.env.ALGOLIA_API_KEY;
  const appId = process.env.ALGOLIA_APP_ID;
  const types = process.env.TYPES;
  const env = process.env.ENVIRONMENT;

  if (!apiKey || !appId) {
    console.error("No ALGOLIA_API_KEY or ALGOLIA_APP_ID provided");
    return;
  }

  if (!types) {
    console.error("No TYPES provided to fetch");
    return;
  }

  if (!env) {
    console.error("No ENVIRONMENT provided to fetch");
    return;
  }

  const parsedTypes = types.split(",").map((type) => type.trim());

  const client = algoliasearch(appId, apiKey);
  const fetchItems = createFetchItems(client);

  await Promise.all(
    parsedTypes.map(async (type) => {
      const items = await fetchItems(index);
      saveDataForType(type, env, items);
    })
  );
}

(async () => {
  try {
    await run();
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();

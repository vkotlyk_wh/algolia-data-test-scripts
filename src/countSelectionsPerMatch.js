const fs = require("fs");
const { getDataForType, saveResults } = require("./utils");

require("dotenv").config({ path: __dirname + "/../.env" });

async function run() {
  const env = process.env.ENVIRONMENT;
  const matches = getDataForType('match', env);
  const selections = getDataForType('selection', env);

  const result = matches.reduce((r, match) => {
    const matchingSelections = selections.filter(selection => selection.event.id === match.objectID);

    r[match.objectID] = matchingSelections.length;
    return r;
  }, {});

  saveResults('selections-count-for-each-match.json', JSON.stringify(result, null, 2));
}

(async () => {
  try {
    await run();
  } catch (e) {
    console.error(e);
  }
})();

const fs = require("fs");
const path = require('path');

function composeDataFilePathForType(type, env) {
  return path.resolve(__dirname, `../data/as-${env}-${type}-en-gb.json`);
}

module.exports = {
  saveDataForType(type, env, data) {
    fs.writeFileSync(
      composeDataFilePathForType(type, env),
      JSON.stringify(data, null, 2)
    );
  },
  getDataForType(type, env) {
    return JSON.parse(fs.readFileSync(composeDataFilePathForType(type, env)));
  },
  saveResults(filename, data) {
    fs.writeFileSync(path.resolve(__dirname, `../results/${filename}`), data);
  }
};
